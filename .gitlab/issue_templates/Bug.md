### Funzionalità impattata
Mettere qui la Descrizione

### Environment
Descrivere l'environment dove si è riprodotto il bug, ad esempio:

* OS
* database
* Versione dell'applicativo
* Device
* Connessione

### Passi per riprodurre il bug
I passi per riprodurre il bug

### Risultati attesi
Descrivere cosa sarebbe dovuto accadere senza il bug

### Risultati effettivi a causa del bug
Descrivere cosa accade in realtà a causa del bug

### Prove del bug (screenshots, videos, text)
Inserire tutta la documentazione utile a dimostrare il bug
