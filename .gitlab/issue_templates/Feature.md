### Descrizione dettagliata della funzionalità
Mettere qui la descrizione della funzionalità che si desidera implementare

### Informazioni contestuali
Informazioni di contesto utili a chi dovrà implementare la feature come:

* Perché è importante?
* Chi e come la dovrà usare?
* Quali benefici porta?
* E' legata ad un'analisi particolare? Se si, mettere collegamento

### Ipotesi di implementazione
Non è obbligatorio, ma si può suggerire un'idea per l'implementazione

### Issues eventuali di riferimento
Altri issues di riferimento

### Note utili (mockups, screenshots, videos, text)
Inserire tutta la documentazione utile a dimostrare che cosa deve fare la funzionalità, ad esempio

### Commessa di riferimento
La commessa ed il modulo di riferimento sulla COIN (se pertinente)
