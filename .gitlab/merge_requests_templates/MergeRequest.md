#### Che cambiamenti introduce la MR?
Indicare il tipo di funzionalità introdotta

#### Indicazioni per un code-reviewer
Indicare da dove un code-reviewer deve partire per analizzare le modifiche al codice

#### Come deve essere testata (anche manualmente)?
Indicare i passi per testare la funzionalità

#### Background context
Indicazioni su eventuali informazioni contestuali utili a chi dovrà testare/fare review/effettuare la merge request

#### Issues di riferimento
Indicare issues di riferimento

### Manutenibilità & Sicurezza

- [ ] La funzionalità è testata
- [ ] La merge request è "compliant" con gli standar di sicurezza
- [ ] Il `code coverage` è maggiore dell' 80%

#### Schermate (se appropriato)
Includere eventuali schermate