Dir.glob '../app/views/foo/**/*' do |file|
  if !Dir.exist? file
    text = File.read file
    text.gsub! /(\w*)foo_(\w+)path/, '\1\2path'

    File.open(file, 'w')  { |f| f.write text }

    new_path = file.gsub "foo/", ""

    dir_to_move_to = new_path.gsub /.+\//, ""

    Dir.mkdir dir_to_move_to unless Dir.exists? dir_to_move_to

    `mv #{file} #{new_path}`
  end
end